import csv
import os
import mysql.connector
from mysql.connector import errorcode
import logging

logger = logging.getLogger(__name__)

file_handler = logging.FileHandler("csvloader.log")
formatter = logging.Formatter("%(asctime)s:%(name)s:%(message)s")
file_handler.setFormatter(formatter)
file_handler.setLevel(logging.WARNING)
logger.addHandler(file_handler)

os.chdir("/Users/waals/Downloads/")
environment_variables = os.environ
db_name = environment_variables["DB_NAME"]
db_pwd = environment_variables["DB_PWD"]
db_host = environment_variables["DB_URL"]
db_engine = "dev1"

# dict_list = list()

with open("premier_league_all_matches.csv", "r") as fr:
    csv_data = csv.DictReader(fr)

    # for line in csv_data:
    #     dict_list.append(line)
    ################################################
    # Using List comprehension instead of the above
    ################################################
    dict_list = [line for line in csv_data]

    try:
        cnx = mysql.connector.connect(user=db_name, password=db_pwd, host=db_host)
    except mysql.connector.Error as err:
        logger.exception("Unable to connect to database")
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            logging.warning("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            logging.warning("Database does not exist")
        else:
            print(err)
    else:
        cursor = cnx.cursor()

        def create_database(cursor):
            try:
                cursor.execute(
                    "CREATE DATABASE {} DEFAULT CHARACTER SET 'utf8'".format(db_engine)
                )
            except mysql.connector.Error as err:
                print("Failed creating database: {}".format(err))
                exit(1)

        try:
            cursor.execute("USE {}".format(db_engine))
        except mysql.connector.Error as err:
            print("Database {} does not exists.".format(db_engine))
            if err.errno == errorcode.ER_BAD_DB_ERROR:
                create_database(cursor)
                print("Database {} created successfully.".format(db_engine))
                cnx.database = db_engine
            else:
                print(err)
                exit(1)

            cnx.database = db_engine
            drop_tables = {
                "Premier_league_drop": "Drop table if exist Premier_league",
                "employees_drop": "Drop table if exist employees",
                "departments_drop": "Drop table if exist departments",
            }

            for dropped_table_names in drop_tables:
                dropped_table_ddl = drop_tables[dropped_table_names]

            tables = {
                "Premier_league": (
                    "create table Premier_league(Week float,Date timestamp,Time time,Home_Team varchar(50),"
                    "Home_xG float,Score varchar(50),Away_xG float,Away_Team varchar(50),Attendance float,"
                    "Venue varchar(50),Referee varchar(50))"
                    "ENGINE=InnoDB"
                ),
                "employees": (
                    "CREATE TABLE `employees` ("
                    "  `emp_no` int(11) NOT NULL AUTO_INCREMENT,"
                    "  `birth_date` date NOT NULL,"
                    "  `first_name` varchar(14) NOT NULL,"
                    "  `last_name` varchar(16) NOT NULL,"
                    "  `gender` enum('M','F') NOT NULL,"
                    "  `hire_date` date NOT NULL,"
                    "  PRIMARY KEY (`emp_no`)"
                    ") ENGINE=InnoDB"
                ),
                "departments": (
                    "CREATE TABLE `departments` ("
                    "  `dept_no` char(4) NOT NULL,"
                    "  `dept_name` varchar(40) NOT NULL,"
                    "  PRIMARY KEY (`dept_no`), UNIQUE KEY `dept_name` (`dept_name`)"
                    ") ENGINE=InnoDB"
                ),
            }
            for table_name in tables:
                table_description = tables[table_name]
                try:
                    print("Creating table {}: ".format(table_name), end="")
                    cursor.execute(dropped_table_ddl)
                    cursor.execute(table_description)
                except mysql.connector.Error as err:
                    if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
                        print("already exists.")
                    else:
                        print(err.msg)
                else:
                    print("OK")

            cursor.close()
            cnx.close()

    for dic_item in dict_list:
        query = "insert into Premier_league (Week,Date,Time,Home_Team,Home_xG,Score,Away_xG,Away_Team,Attendance,Venue,Referee) values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
        val = (
            dic_item["Week"],
            dic_item["Date"],
            dic_item["Time"],
            dic_item["Home_Team"],
            dic_item["Home_xG"],
            dic_item["Score"],
            dic_item["Away_xG"],
            dic_item["Away_Team"],
            dic_item["Attendance"],
            dic_item["Venue"],
            dic_item["Referee"],
        )
        # print(val)
        cnx.commit()
        logging.warning("Inserting ......")
    cnx.close()
