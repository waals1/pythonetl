import os
from datetime import datetime, timedelta, date

import mysql.connector
from mysql.connector import errorcode
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.WARNING)
formatter = logging.Formatter("%(asctime)s:%(name)s:%(message)s")
file_handler = logging.FileHandler("EmployeeDB.log")
file_handler.setLevel(logging.WARNING)
file_handler.setFormatter(formatter)

logger.addHandler(file_handler)


db_url = os.environ.get("DB_URL")
db_name = os.environ.get("DB_NAME")
db_pwd = os.environ.get("DB_PWD")

tomorrow = datetime.now().date() + timedelta(days=1)
try:
    mysql_connection = mysql.connector.connect(
        user=db_name, host=db_url, password=db_pwd, database="employees"
    )
except mysql.connector.Error as err:
    logger.exception(
        "Failed to connect to the database, check your credentials or status of database"
    )
    if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
        logger.exception("Something is wrong with your user name or password")
    elif err.errno == errorcode.ER_BAD_DB_ERROR:
        logger.exception("Database does not exist")
    else:
        print(err)
    exit(1)
else:
    mysql_cursor = mysql_connection.cursor()
    employee_query = "INSERT INTO employees (first_name, last_name, hire_date, gender, birth_date) VALUES (%s, %s, %s, %s, %s)"
    data_employee = ("Geert", "Vanderkelen", tomorrow, "M", date(1977, 6, 14))

    salary_query = (
        "INSERT INTO salaries "
        "(emp_no, salary, from_date, to_date) "
        "VALUES (%(emp_no)s, %(salary)s, %(from_date)s, %(to_date)s)"
    )
    emp_no = mysql_cursor.lastrowid
    data_salary = {
        "emp_no": emp_no,
        "salary": 50000,
        "from_date": tomorrow,
        "to_date": date(9999, 1, 1),
    }
    try:
        mysql_cursor.execute(employee_query, data_employee)
        mysql_connection.commit()
    except logger.warning(mysql_cursor.execute(salary_query, data_salary)):
        mysql_connection.commit()

    mysql_connection.close()
    print("Inserted successfully and connection closed")
