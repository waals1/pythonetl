import json
import os
import sys


import mysql.connector
from mysql.connector import errorcode
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.WARNING)

formatter = logging.Formatter("%(asctime)s:%(name)s:%(message)s")
file_handler = logging.FileHandler("JsonLoader.log")
file_handler.setFormatter(formatter)
file_handler.setLevel(logging.WARNING)
logger.addHandler(file_handler)

os.chdir("/Users/waals/Downloads/archive-2/data/")
db_url = os.environ.get("DB_URL")
db_name = os.environ.get("DB_NAME")
db_pwd = os.environ.get("DB_PWD")
db_env = "dev"

# json_dict = []
# with open("creditcard_json.json", "r") as rf:
#    json_data = json.load(rf)
#    for rows in json_data:
#        json_dict.append(rows)
########################################################
# using list comprehension instead of for loop  above
########################################################
with open("creditcard_json.json", "r") as rf:
    json_data = json.load(rf)
    json_dict = [rows for rows in json_data]

    # print(json_dict)
    try:
        mysql_connection = mysql.connector.connect(
            user=db_name, host=db_url, password=db_pwd, database=db_env
        )
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)
            exit(1)
    else:
        mysql_cursor = mysql_connection.cursor()
    finally:
        print("Database connected successfully !!!")

    table_name_drop = {"JsonTable_drop": "drop table if exists credit_card"}

    table_name = {
        "JsonTable": "create table credit_card(Amount float, Class varchar(5),Time float,"
        " V1 float, V10 float, V11 float, V12 float , V13 float , V14 float, V15 float, V16 float, V17 float, V18 float, V19 float, V2 float, V20 float , V21 float, V22 float, V23 float, V24 float, V25 float, V26 float, V27 float, V28 float, V3 float, V4 float, V5 float,V6 float, V7 float, V8 float, V9 float)"
    }

    JsonTable_drop_ddl = table_name_drop["JsonTable_drop"]
    credit_card_table_ddl = table_name["JsonTable"]

    mysql_cursor.execute(JsonTable_drop_ddl)
    mysql_cursor.execute(credit_card_table_ddl)

    for line in json_dict:
        val = (
            line.get("Amount"),
            line.get("Class"),
            line.get("Time"),
            line.get("V1"),
            line.get("V10"),
            line.get("V11"),
            line.get("V12"),
            line.get("V13"),
            line.get("V14"),
            line.get("V15"),
            line.get("V16"),
            line.get("V17"),
            line.get("V18"),
            line.get("V19"),
            line.get("V2"),
            line.get("V20"),
            line.get("V21"),
            line.get("V22"),
            line.get("V23"),
            line.get("V24"),
            line.get("V25"),
            line.get("V26"),
            line.get("V27"),
            line.get("V28"),
            line.get("V3"),
            line.get("V4"),
            line.get("V5"),
            line.get("V6"),
            line.get("V7"),
            line.get("V8"),
            line.get("V9"),
        )

        # print(val)
        query = (
            "insert into credit_card (Amount,Class,Time, V1, V10, V11, V12 , V13 , V14, V15, V16, V17, V18, V19, V2, V20 , V21, V22, V23, V24, V25, V26, V27, V28, V3, V4, V5,V6, V7, V8, V9)"
            "VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
        )
        logger.warning(mysql_cursor.execute(query, val))
        mysql_connection.commit()
    mysql_connection.close()
