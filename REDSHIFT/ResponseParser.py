import json

try:
    with open("test1.json", "r") as rf:
        json_data = json.load(rf)
except FileNotFoundError as e:
    print(e)
except Exception as ee:
    print(ee)
else:
    for line in json_data["Clusters"]:
        print(line["ClusterIdentifier"])
