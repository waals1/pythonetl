import json
import redshift_connector
import time
import csv
from REDSHIFT.my_redshift_services import aws_services as rs


start = time.perf_counter()

mycluster = input("Enter a name for you Cluster Identifier: ")


if rs.check_cluster() != mycluster:
    rs.create_cluster(
        "dev",
        mycluster,
        "multi-node",
        "dc2.large",
        rs.Redshift_user,
        rs.Redshift_pwd,
    )

    time.sleep(180)
else:
    print(f"my cluster is up an running")

try:
    redshift_conn = redshift_connector.connect(
        user=rs.Redshift_user,
        password=rs.Redshift_pwd,
        host="my-cluster.ce1nnnxig2yl.us-east-1.redshift.amazonaws.com",
        port=5439,
        database="dev",
    )
except redshift_connector.Error as err:
    print(err)
else:
    redshift_cursor = redshift_conn.cursor()
    rs.logger.info("connection successful")

    drop_table_ddl = {
        "Premier_league": "drop table if exists Premier_league",
        "creditcard_json": "drop table if exists credit_card",
    }

    create_table = {
        "Premier_league": (
            "create table Premier_league(Week float,Date timestamp,Time time,Home_Team varchar(50),"
            "Home_xG float,Score varchar(50),Away_xG float,Away_Team varchar(50),Attendance float,"
            "Venue varchar(50),Referee varchar(50))"
        ),
        "creditcard_json": (
            "create table credit_card(Amount float, Class varchar(5),Time float,"
            " V1 float, V10 float, V11 float, V12 float , V13 float , V14 float, V15 float, V16 float, V17 float, V18 float, V19 float, V2 float, V20 float , V21 float, V22 float, V23 float, V24 float, V25 float, V26 float, V27 float, V28 float, V3 float, V4 float, V5 float,V6 float, V7 float, V8 float, V9 float)"
        ),
    }
    premier_league_drop = drop_table_ddl["Premier_league"]
    redshift_cursor.execute(premier_league_drop)
    rs.logger.info("Checking if premier league table exist....")

    premier_league_ddl = create_table["Premier_league"]
    redshift_cursor.execute(premier_league_ddl)
    rs.logger.info("Premier league table created....")

    credit_card_drop = drop_table_ddl["creditcard_json"]
    redshift_cursor.execute(credit_card_drop)
    print("Checking if creditcard_json table exist....")

    credit_card_ddl = create_table["creditcard_json"]
    redshift_cursor.execute(credit_card_ddl)
    print("Credit card table created")

    with open("/Users/waals/Downloads/premier_league_all_matches.csv", "r") as rf:
        csv_data = csv.DictReader(rf)

        for line in csv_data:
            query = "insert into Premier_league (Week,Date,Time,Home_Team,Home_xG,Score,Away_xG,Away_Team,Attendance,Venue,Referee) values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
            # noinspection PyTypeChecker
            val = [
                (
                    line["Week"],
                    line["Date"],
                    line["Time"],
                    line["Home_Team"],
                    line["Home_xG"],
                    line["Score"],
                    line["Away_xG"],
                    line["Away_Team"],
                    line["Attendance"],
                    line["Venue"],
                    line["Referee"],
                )
            ]

            redshift_cursor.executemany(query, val)
            print("inserting records into Premier_league table >>>>>>>>>>>>>>>")
            redshift_conn.commit()
        print("Insert completed")

        # redshift_cursor.execute(
        #     "unload ('select * from Premier_league') to 's3://waalsbucket/premier_league.csv'  iam_role 'arn:aws:iam::136065939800:role/service-role/AmazonRedshift-CommandsAccessRole-20230914T125622' csv;"
        # )

    with open("/Users/waals/Downloads/archive-2/data/creditcard_json.json", "r") as Jrf:
        json_data = json.load(Jrf)

        for line in json_data:
            query2 = (
                "insert into credit_card (Amount,Class,Time, V1, V10, V11, V12 , V13 , V14, V15, V16, V17, V18, V19, V2, V20 , V21, V22, V23, V24, V25, V26, V27, V28, V3, V4, V5,V6, V7, V8, V9)"
                "VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
            )
            val = [
                (
                    line.get("Amount"),
                    line.get("Class"),
                    line.get("Time"),
                    line.get("V1"),
                    line.get("V10"),
                    line.get("V11"),
                    line.get("V12"),
                    line.get("V13"),
                    line.get("V14"),
                    line.get("V15"),
                    line.get("V16"),
                    line.get("V17"),
                    line.get("V18"),
                    line.get("V19"),
                    line.get("V2"),
                    line.get("V20"),
                    line.get("V21"),
                    line.get("V22"),
                    line.get("V23"),
                    line.get("V24"),
                    line.get("V25"),
                    line.get("V26"),
                    line.get("V27"),
                    line.get("V28"),
                    line.get("V3"),
                    line.get("V4"),
                    line.get("V5"),
                    line.get("V6"),
                    line.get("V7"),
                    line.get("V8"),
                    line.get("V9"),
                )
            ]
            redshift_cursor.executemany(query2, val)
            print("inserting records into credit_card table>>>>>>>>>>>>>>>>>>>>>>>")
        print("insert completed")
        redshift_conn.commit()

        redshift_conn.close()

        rs.stop_cluster(mycluster)
        print("Deleting Cluster !!!")

end = time.perf_counter()
duration = round(end - start, 2)
print(f"Finished at {duration}")
