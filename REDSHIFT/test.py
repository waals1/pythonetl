import os

from REDSHIFT.my_redshift_services import aws_services

# drop_table_ddl = {
#     "Premier_league": "drop table if exists Premier_league",
#     "creditcard_json": "drop table if exists creditcard_json",
# }
#
# premier_league_dll = drop_table_ddl["Premier_league"]
# credit_card_ddl = drop_table_ddl["creditcard_json"]
#
# print(premier_league_dll)
# print(credit_card_ddl)

# aws_services.create_cluster(
#     "dev",
#     "my-cluster",
#     "multi-node",
#     "dc2.large",
#     aws_services.Redshift_user,
#     aws_services.Redshift_pwd,
# )
#
aws_services.stop_cluster("my-cluster")

# print(aws_services.check_cluster())
# import logging
# import os
# import boto3
#
# logger = logging.getLogger(__name__)
# file_handler = logging.FileHandler("RCSVLoader.log")
# formatter = logging.Formatter("%(asctime)s:%(name)s:%(message)s")
# file_handler.setFormatter(formatter)
# file_handler.setLevel(logging.WARNING)
# logger.addHandler(file_handler)
#
#
# class aws_services:
#     ClusterIdentifier = input("Name of cluster : ")
#     Redshift_user = os.environ.get("Redshift_user")
#     Redshift_pwd = os.environ.get("Redshift_pwd")
#     Redshift_host = os.environ.get("Redshift_host")
#     database = "dev"
#     port = 1539
#
#     def __int__(
#         self,
#         dbname,
#         cluster_identifier,
#         cluster_type,
#         node_type,
#         master_username,
#         master_user_password,
#     ):
#         self.dbname = dbname
#         self.cluster_identifier = cluster_identifier
#         self.cluster_type = cluster_type
#         self.node_type = node_type
#         self.master_username = master_username
#         self.master_user_password = master_user_password
#
#
# def create_cluster(self,dbname,cluster_identifier,cluster_type,node_type,master_username,master_user_password):
#     try:
#         client = boto3.client("redshift")
#     except Exception as e:
#         print(e)
#     else:
#         client.create_cluster(
#             DBName=dbname,
#             ClusterIdentifier=cluster_identifier,
#             ClusterType="multi-node",
#             NodeType="dc2.large",
#             MasterUsername=Redshift_user,
#             MasterUserPassword=Redshift_pwd,
#             VpcSecurityGroupIds=["sg-07c791b33f387ddc5"],
#             ClusterSubnetGroupName="cluster-subnet-group-1",
#             ClusterParameterGroupName="default.redshift-1.0",
#             Port=5439,
#             AllowVersionUpgrade=False,
#             NumberOfNodes=2,
#             PubliclyAccessible=True,
#             Encrypted=False,
#             Tags=[
#                 {"Key": "MyRedshift", "Value": "Test Cluster"},
#             ],
#             EnhancedVpcRouting=False,
#             IamRoles=[
#                 "arn:aws:iam::136065939800:role/service-role/AmazonRedshift-CommandsAccessRole-20230914T125622",
#                 "arn:aws:iam::136065939800:role/aws-service-role/redshift.amazonaws.com/AWSServiceRoleForRedshift",
#             ],
#             AvailabilityZoneRelocation=False,
#             AquaConfigurationStatus="disabled",
#             DefaultIamRoleArn="arn:aws:iam::136065939800:role/service-role/AmazonRedshift-CommandsAccessRole-20230914T125622",
#         )
#         # finally:
#         print(f"Starting my cluster {ClusterIdentifier}")
#
#
# def stop_cluster():
#     try:
#         client = boto3.client("redshift")
#     except Exception as e:
#         logger.exception(e)
#     else:
#         client.delete_cluster(
#             ClusterIdentifier=ClusterIdentifier, SkipFinalClusterSnapshot=True
#         )
#         # finally:
#         print(f"Deleting my cluster {ClusterIdentifier}............")
#
#
# def check_cluster():
#     try:
#         client = boto3.client("redshift")
#     except Exception as e:
#         print(e)
#     else:
#         response = client.describe_clusters()
#         for row in response["Clusters"]:
#             return row["ClusterIdentifier"]
#         print(f"checking my cluster {ClusterIdentifier}............")
