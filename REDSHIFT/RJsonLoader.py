import logging
import os
import time
import json
import concurrent.futures
import boto3


import redshift_connector

ClusterIdentifier = "my-cluster"
Redshift_user = os.environ.get("Redshift_user")
Redshift_pwd = os.environ.get("Redshift_pwd")
Redshift_host = os.environ.get("Redshift_host")

logger = logging.getLogger(__name__)
file_handler = logging.FileHandler("RCSVLoader.log")
formatter = logging.Formatter("%(asctime)s:%(name)s:%(message)s")
file_handler.setFormatter(formatter)
file_handler.setLevel(logging.WARNING)
logger.addHandler(file_handler)


def start_cluster():
    try:
        client = boto3.client("redshift")
    except Exception as e:
        print(e)
    else:
        client.create_cluster(
            DBName="dev",
            ClusterIdentifier=ClusterIdentifier,
            ClusterType="multi-node",
            NodeType="dc2.large",
            MasterUsername=Redshift_user,
            MasterUserPassword=Redshift_pwd,
            VpcSecurityGroupIds=[
                "sg-07c791b33f387ddc5",
            ],
            ClusterSubnetGroupName="cluster-subnet-group-1",
            ClusterParameterGroupName="default.redshift-1.0",
            Port=5439,
            AllowVersionUpgrade=False,
            NumberOfNodes=2,
            PubliclyAccessible=True,
            Encrypted=False,
            Tags=[
                {"Key": "MyRedshift", "Value": "Test Cluster"},
            ],
            EnhancedVpcRouting=False,
            IamRoles=[
                "arn:aws:iam::136065939800:role/service-role/AmazonRedshift-CommandsAccessRole-20230914T125622",
                "arn:aws:iam::136065939800:role/aws-service-role/redshift.amazonaws.com/AWSServiceRoleForRedshift",
            ],
            AvailabilityZoneRelocation=False,
            AquaConfigurationStatus="disabled",
            DefaultIamRoleArn="arn:aws:iam::136065939800:role/service-role/AmazonRedshift-CommandsAccessRole-20230914T125622",
        )
        # finally:
        print(f"Starting. my cluster {ClusterIdentifier}............")


def stop_cluster():
    try:
        client = boto3.client("redshift")
    except Exception as e:
        logger.exception(e)
    else:
        client.delete_cluster(
            ClusterIdentifier=ClusterIdentifier, SkipFinalClusterSnapshot=True
        )
        # finally:
        print("Deleting Cluster................")


def check_cluster():
    try:
        client = boto3.client("redshift")
    except Exception as e:
        print(e)
    else:
        response = client.describe_clusters()
        for row in response["Clusters"]:
            return row["ClusterIdentifier"]


start = time.perf_counter()

if __name__ == "__main__":
    start = time.perf_counter()
    if check_cluster() != ClusterIdentifier:
        start_cluster()
        print("Starting Cluster...........")
        time.sleep(180)
    else:
        print(f"my {ClusterIdentifier} is up an running")

    try:
        redshift_conn = redshift_connector.connect(
            user=Redshift_user,
            password=Redshift_pwd,
            host="my-cluster.ce1nnnxig2yl.us-east-1.redshift.amazonaws.com",
            port=5439,
            database="dev",
        )
    except redshift_connector.Error as err:
        print(err)
    else:
        redshift_cursor = redshift_conn.cursor()
        print("connection successful")

        drop_table = "drop table if exists credit_card"

        redshift_cursor.execute(drop_table)

        print("Checking if table exist....")

        table_name = {
            "creditcard_json": (
                "create table credit_card(Amount float, Class varchar(5),Time float,"
                " V1 float, V10 float, V11 float, V12 float , V13 float , V14 float, V15 float, V16 float, V17 float, V18 float, V19 float, V2 float, V20 float , V21 float, V22 float, V23 float, V24 float, V25 float, V26 float, V27 float, V28 float, V3 float, V4 float, V5 float,V6 float, V7 float, V8 float, V9 float)"
            )
        }

        table_ddl = table_name["creditcard_json"]

        redshift_cursor.execute(table_ddl)
        redshift_conn.commit()

        with open(
            "/Users/waals/Downloads/archive-2/data/creditcard_json.json", "r"
        ) as rf:
            json_data = json.load(rf)

        for line in json_data:
            query = (
                "insert into credit_card (Amount,Class,Time, V1, V10, V11, V12 , V13 , V14, V15, V16, V17, V18, V19, V2, V20 , V21, V22, V23, V24, V25, V26, V27, V28, V3, V4, V5,V6, V7, V8, V9)"
                "VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
            )
            val = [
                (
                    line.get("Amount"),
                    line.get("Class"),
                    line.get("Time"),
                    line.get("V1"),
                    line.get("V10"),
                    line.get("V11"),
                    line.get("V12"),
                    line.get("V13"),
                    line.get("V14"),
                    line.get("V15"),
                    line.get("V16"),
                    line.get("V17"),
                    line.get("V18"),
                    line.get("V19"),
                    line.get("V2"),
                    line.get("V20"),
                    line.get("V21"),
                    line.get("V22"),
                    line.get("V23"),
                    line.get("V24"),
                    line.get("V25"),
                    line.get("V26"),
                    line.get("V27"),
                    line.get("V28"),
                    line.get("V3"),
                    line.get("V4"),
                    line.get("V5"),
                    line.get("V6"),
                    line.get("V7"),
                    line.get("V8"),
                    line.get("V9"),
                )
            ]
            redshift_cursor.executemany(query, val)
            print("inserting >>>>>>>>>>>>>>>>>>>>>>>")
        print("insert completed")
        redshift_conn.commit()

        redshift_conn.close()


stop_cluster()
stop = time.perf_counter()
job_duration = stop - start

print(f" Job ran for {job_duration}")
