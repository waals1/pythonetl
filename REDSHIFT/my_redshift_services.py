import logging
import os
import boto3


class aws_services:
    logger = logging.getLogger(__name__)
    file_handler = logging.FileHandler("create_cluster.log")
    formatter = logging.Formatter("%(asctime)s:%(name)s:%(message)s")
    file_handler.setFormatter(formatter)
    file_handler.setLevel(logging.WARNING)
    logger.addHandler(file_handler)

    Redshift_user = os.environ.get("Redshift_user")
    Redshift_pwd = os.environ.get("Redshift_pwd")
    Redshift_host = os.environ.get("Redshift_host")
    database = "dev"
    port = 1539

    @classmethod
    def create_cluster(
        cls,
        dbname,
        cluster_identifier,
        cluster_type,
        node_type,
        master_username,
        master_user_password,
    ):
        try:
            client = boto3.client("redshift")
        except Exception as e:
            print(e)
        else:
            client.create_cluster(
                DBName=dbname,
                ClusterIdentifier=cluster_identifier,
                ClusterType=cluster_type,
                NodeType=node_type,
                MasterUsername=master_username,
                MasterUserPassword=master_user_password,
                VpcSecurityGroupIds=["sg-07c791b33f387ddc5"],
                ClusterSubnetGroupName="cluster-subnet-group-1",
                ClusterParameterGroupName="default.redshift-1.0",
                Port=5439,
                AllowVersionUpgrade=False,
                NumberOfNodes=2,
                PubliclyAccessible=True,
                Encrypted=False,
                Tags=[
                    {"Key": "MyRedshift", "Value": "Test Cluster"},
                ],
                EnhancedVpcRouting=False,
                IamRoles=[
                    "arn:aws:iam::136065939800:role/service-role/AmazonRedshift-CommandsAccessRole-20230914T125622",
                    "arn:aws:iam::136065939800:role/aws-service-role/redshift.amazonaws.com/AWSServiceRoleForRedshift",
                ],
                AvailabilityZoneRelocation=False,
                AquaConfigurationStatus="disabled",
                DefaultIamRoleArn="arn:aws:iam::136065939800:role/service-role/AmazonRedshift-CommandsAccessRole-20230914T125622",
            )
            # finally:
            print(f"Starting my cluster {cluster_identifier}")

    @classmethod
    def stop_cluster(cls, cluster_identifier):
        logger = logging.getLogger(__name__)
        file_handler = logging.FileHandler("stop_cluster.log")
        formatter = logging.Formatter("%(asctime)s:%(name)s:%(message)s")
        file_handler.setFormatter(formatter)
        file_handler.setLevel(logging.WARNING)
        logger.addHandler(file_handler)
        try:
            client = boto3.client("redshift")
        except Exception as e:
            logger.exception(e)
        else:
            client.delete_cluster(
                ClusterIdentifier=cluster_identifier, SkipFinalClusterSnapshot=True
            )
            # finally:
            print(f"Deleting my cluster {cluster_identifier}............")

    @staticmethod
    def check_cluster():
        logger = logging.getLogger(__name__)
        file_handler = logging.FileHandler("check_cluster.log")
        formatter = logging.Formatter("%(asctime)s:%(name)s:%(message)s")
        file_handler.setFormatter(formatter)
        file_handler.setLevel(logging.WARNING)
        logger.addHandler(file_handler)
        try:
            client = boto3.client("redshift")
        except Exception as e:
            print(e)
        else:
            response = client.describe_clusters()
            for row in response["Clusters"]:
                return row["ClusterIdentifier"]

    @staticmethod
    def start_emr():
        client = boto3.client("emr")
        response = client.start_emr()
