import pandas as pd
import os
import logging
from REDSHIFT.my_redshift_services import aws_services


logger = logging.getLogger(__name__)
file_handler = logging.FileHandler("DataFrameLoader.log")
file_formatter = logging.Formatter("%(asctime)s:%(name)s:%(message)s")
file_handler.setFormatter(file_formatter)
logger.setLevel(logging.WARNING)
logger.addHandler(file_handler)

Redshift_user = os.environ.get("Redshift_user")
Redshift_pwd = os.environ.get("Redshift_pwd")
Redshift_host = os.environ.get("Redshift_host")
database = "dev"
port = 1539

# pd.set_option("display.max_columns", 31)
# pd.set_option("display.max_rows", 31)

datafile = pd.read_json("/Users/waals/Downloads/archive-2/data/creditcard_json.json")
# print(datafile[["V7", "V8"]])


print(datafile.iloc[[0, 1], 2])
print(datafile.loc[[0, 1], ["Amount", "Time"]])
print(datafile.head(20))
